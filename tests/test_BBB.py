from django.test import TestCase
from django.urls import reverse
from datetime import date

from erp_v_i.models import Categoria, Producto, Cliente, Venta, Detalle_De_Venta
from erp_v_i.views import Panel_Principal

class ModelsTestCase(TestCase):
    def setUp(self):
        self.categoria1 = Categoria.objects.create(nombre="Categoria1", descripcion="Descripcion de la categoria1")
        self.producto1 = Producto.objects.create(nombre="Producto1", categoria=self.categoria1, stock=0, pvp=10.50)
        self.cliente1 = Cliente.objects.create(cedula_de_identidad="1234567", nombres="John", apellidos="Doe", telefono="555-5555", fecha_de_nacimiento=date(1999, 1, 1), direccion="123 Main St.", informacion_extra="Some extra info")
        self.venta1 = Venta.objects.create(cliente=self.cliente1, comentario="Test comment", total=10.50)
        self.detalle_de_venta1 = Detalle_De_Venta.objects.create(venta=self.venta1, producto=self.producto1, cantidad=1)

    def test_categoria(self):
        categoria = Categoria.objects.get(nombre="Categoria1")
        self.assertEqual(str(categoria), "Categoria1")
        self.assertEqual(categoria.descripcion, "Descripcion de la categoria1")

    def test_producto(self):
        producto = Producto.objects.get(nombre="Producto1")
        self.assertEqual(str(producto), "Producto1 $10.50")
        self.assertEqual(producto.categoria, self.categoria1)
        self.assertEqual(producto.stock, 0)

    def test_cliente(self):
        cliente = Cliente.objects.get(cedula_de_identidad="1234567")
        self.assertEqual(str(cliente), "1234567")
        self.assertEqual(cliente.nombres, "John")
        self.assertEqual(cliente.apellidos, "Doe")
        self.assertEqual(cliente.telefono, "555-5555")
        self.assertEqual(cliente.fecha_de_nacimiento, date(1999, 1, 1))
        self.assertEqual(cliente.direccion, "123 Main St.")
        self.assertEqual(cliente.informacion_extra, "Some extra info")

    def test_venta(self):
        venta = Venta.objects.get(comentario="Test comment")
        self.assertEqual(venta.cliente, self.cliente1)
        self.assertEqual(venta.total, 10.50)

    def test_detalle_de_venta(self):
        detalle_de_venta = Detalle_De_Venta.objects.get(venta=self.venta1)
        self.assertEqual(detalle_de_venta.producto, self.producto1)
        self.assertEqual(detalle_de_venta.cantidad, 1)
