from django.test import TestCase, Client
from django.urls import reverse
from usuario.models import UsuarioPersonalizado
from login.urls import Inicio_De_Sesion
from django.contrib.auth.hashers import make_password

class InicioDeSesionTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.username = 'test_user'
        self.password = 'test_password'
        self.user = UsuarioPersonalizado.objects.create(
            username=self.username,
            password=make_password(self.password),
        )

    def test_inicio_de_sesion_exitoso(self):
        print(reverse('Iniciar_Sesion'))
        response = self.client.post(reverse('Iniciar_Sesion'), {
            'username': self.username,
            'password': self.password
        })
        self.assertRedirects(response, '/erp/panel-principal/')
