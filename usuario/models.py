from django.db import models
from django.contrib.auth.models import AbstractUser

# creamos una nueva clase usario la cual hereda atributos base de AbstractUser ,y  le implementamos dos campos adicionales
class UsuarioPersonalizado(AbstractUser):
    imagen = models.ImageField(upload_to='imagen_de_usuario/%Y/%m/%d', null=True, blank=True)
    telefono = models.CharField(max_length=20, null=True, blank=True, verbose_name="Telefono")



