from django.contrib import admin
from .models import UsuarioPersonalizado
from .forms import FormularioUsuarioPersonalizado
from django.contrib.auth.admin import UserAdmin


class UsuarioPersonalizadoAdmin(UserAdmin):
    model = UsuarioPersonalizado
    add_form = FormularioUsuarioPersonalizado

    fieldsets = (*UserAdmin.fieldsets,
        ('Imagen De Usuario Y Telefono', {
            'fields': ('imagen', 'telefono')
        })
    )

admin.site.register(UsuarioPersonalizado,UsuarioPersonalizadoAdmin)
