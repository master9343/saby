from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import UsuarioPersonalizado

class FormularioUsuarioPersonalizado(UserCreationForm):

    class Meta:
        model = UsuarioPersonalizado
        fields = "__all__"

