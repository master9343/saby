from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator 

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
from django.urls import reverse_lazy


from .models import Categoria, Producto, Cliente, Venta, Detalle_De_Venta
from .forms import Categoria_Formulario, Producto_Formulario, Cliente_Formulario, Venta_Formulario, Detalle_De_Venta_FormSet, Detalle_De_Venta_Formulario 

from django.shortcuts import render, redirect

from datetime import date # esto solo se usa para la vista Panel_Principal

def Panel_Principal(request):

    if Producto.objects.exists():
        cantidad_productos_totales = Producto.objects.count()
    else:
        cantidad_productos_totales = 0 

    if Venta.objects.exists():
        cantidad_ventas_totales = Venta.objects.count()
        ultima_venta = Venta.objects.last()
        total_de_ultima_venta = ultima_venta.total
        cantidad_de_ventas_del_dia_actual = Venta.objects.filter(fecha__gte=date.today()).count() # Filtra las Ventas realizadas el dia actual y luego cuenta la cantidad 
    else :
        cantidad_ventas_totales = 0
        ultima_venta = 0
        total_de_ultima_venta = 0
        cantidad_de_ventas_del_dia_actual = 0

    title = "Panel Principal"

    context = {
        "title": title,
        "cantidad_productos_totales": cantidad_productos_totales,
        "cantidad_ventas_totales": cantidad_ventas_totales,
        "total_de_ultima_venta": total_de_ultima_venta,
        "cantidad_de_ventas_del_dia_actual":cantidad_de_ventas_del_dia_actual, 
    }
    return render(request, "0_panel_principal.html", context)

# Para poder proteger una vista basada en clase usando login_required, una forma es sobreescribiendo el metodo dispatch

class Categoria_Lista(PermissionRequiredMixin, ListView):
    model = Categoria
    template_name = "1_lista_categoria.html" 
    permission_required = ("erp_v_i.view_categoria") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    # Este metodo devuelve un diccionario
    # Nos permite aumentar la cantidad de variables que se envian a la platilla
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Lista Categoria"
        context["url_crear"]= reverse_lazy("erp:Categoria_Crear")

        context["entidad"] = "Categorias"
        context["url_lista"] = reverse_lazy("erp:Categoria_Lista")

        return context

class Categoria_Crear(PermissionRequiredMixin,CreateView):
    model = Categoria
    form_class = Categoria_Formulario 
    template_name = "1_crear_categoria.html"
    # esta linea representa que se redirecionara a la url /categoria/lista
    success_url = reverse_lazy("erp:Categoria_Lista")
    permission_required = ("erp_v_i.add_categoria") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Crear Categoria"

        context["entidad"] = "Categorias"
        context["url_lista"] = reverse_lazy("erp:Categoria_Lista")

        return context


class Categoria_Editar(PermissionRequiredMixin,UpdateView):
    model = Categoria
    form_class = Categoria_Formulario 
    template_name = "1_crear_categoria.html"
    success_url = reverse_lazy("erp:Categoria_Lista")
    permission_required = ("erp_v_i.change_categoria") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Editar Categoria"

        context["entidad"] = "Categorias"
        context["url_lista"] = reverse_lazy("erp:Categoria_Lista")

        return context

class Categoria_Eliminar(PermissionRequiredMixin,DeleteView):
    model = Categoria
    template_name = "1_eliminar_categoria.html"
    # Cuando la elimnacion sea satisfactoria se redirecionar la pagina a la url /categoria/lista
    success_url = reverse_lazy("erp:Categoria_Lista")
    permission_required = ("erp_v_i.delete_categoria") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Eliminar Categoria"

        context["entidad"] = "Categorias"
        context["url_lista"] = reverse_lazy("erp:Categoria_Lista")


        return context


class Producto_Lista(PermissionRequiredMixin,ListView):
    model = Producto 
    template_name = "2_lista_producto.html" 
    permission_required = ("erp_v_i.view_producto") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Lista Producto"
        context["url_crear"]= reverse_lazy("erp:Producto_Crear")

        context["entidad"] = "Productos"
        context["url_lista"] = reverse_lazy("erp:Producto_Lista")

        return context


class Producto_Crear(PermissionRequiredMixin,CreateView):
    model = Producto 
    form_class = Producto_Formulario  
    template_name = "2_crear_producto.html"
    # esta linea representa que se redirecionara a la url /producto/lista
    success_url = reverse_lazy("erp:Producto_Lista")
    permission_required = ("erp_v_i.add_producto") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Crear Producto"
        context["entidad"] = "Productos"

        context["url_lista"] = reverse_lazy("erp:Producto_Lista")

        return context


class Producto_Editar(PermissionRequiredMixin,UpdateView):
    model = Producto 
    form_class = Producto_Formulario  
    template_name = "2_crear_producto.html"
    success_url = reverse_lazy("erp:Producto_Lista")
    permission_required = ("erp_v_i.change_producto") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Editar Producto"

        context["entidad"] = "Productos"
        context["url_lista"] = reverse_lazy("erp:Producto_Lista")

        return context


class Producto_Eliminar(PermissionRequiredMixin,DeleteView):
    model = Producto 
    template_name = "2_eliminar_producto.html"
    # Cuando la elimnacion sea satisfactoria se redirecionar la pagina a la url /producto/lista
    success_url = reverse_lazy("erp:Producto_Lista")
    permission_required = ("erp_v_i.delete_producto") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Eliminar Producto"

        context["entidad"] = "Productos"
        context["url_lista"] = reverse_lazy("erp:Producto_Lista")


        return context


# Apartado de Clientes • Inicion 

class Cliente_Lista(PermissionRequiredMixin,ListView):
    model = Cliente 
    template_name = "3_lista_cliente.html" 
    permission_required = ("erp_v_i.view_cliente") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Lista Cliente"
        context["url_crear"]= reverse_lazy("erp:Cliente_Crear")

        context["entidad"] = "Clientes"
        context["url_lista"] = reverse_lazy("erp:Cliente_Lista")

        return context


class Cliente_Crear(PermissionRequiredMixin,CreateView):
    model = Cliente 
    form_class = Cliente_Formulario  
    template_name = "3_crear_cliente.html"
    success_url = reverse_lazy("erp:Cliente_Lista")
    permission_required = ("erp_v_i.add_cliente") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Crear Cliente"
        context["entidad"] = "Clientes"
        context["url_lista"] = reverse_lazy("erp:Cliente_Lista")

        return context


class Cliente_Editar(PermissionRequiredMixin,UpdateView):
    model = Cliente 
    form_class = Cliente_Formulario  
    template_name = "3_crear_cliente.html"
    success_url = reverse_lazy("erp:Cliente_Lista")
    permission_required = ("erp_v_i.change_cliente") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Editar Cliente"
        context["entidad"] = "Clientes"
        context["url_lista"] = reverse_lazy("erp:Cliente_Lista")

        return context


class Cliente_Eliminar(PermissionRequiredMixin,DeleteView):
    model = Cliente 
    template_name = "3_eliminar_cliente.html"
    success_url = reverse_lazy("erp:Cliente_Lista")
    permission_required = ("erp_v_i.delete_cliente") 

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Eliminar Cliente"
        context["entidad"] = "Clientes"
        context["url_lista"] = reverse_lazy("erp:Cliente_Lista")


        return context

@permission_required("erp_v_i.view_cliente",raise_exception=True)
def Cliente_Ver(request,pk):
    cliente_datos = Cliente.objects.get(id=pk)
    title = "Ver Cliente"
    entidad = "Clientes"
    url_lista = reverse_lazy("erp:Cliente_Lista")
    # cliente_datos = cliente_id.__dict__
    # cliente_datos = cliente_id._meta.get_fields()

    context = {
        "cliente_datos":cliente_datos,
        "title":title,
        "entidad":entidad,
        "url_lista":url_lista
    }
    print(cliente_datos)
    print(cliente_datos)

    return render(request, "3_ver_cliente.html", context)

# Apartado de Clientes • Fin 

@permission_required("erp_v_i.add_venta",raise_exception=True)
def Venta_Crear(request):
    form = Venta_Formulario()
    formset = Detalle_De_Venta_FormSet()


    if request.method == "POST":
        form = Venta_Formulario(request.POST)
        formset = Detalle_De_Venta_FormSet(request.POST)

        # si el formulario es valido creara cliente , comentario , total 
        if form.is_valid():
            VENTA = Venta.objects.create(
                 cliente=form.cleaned_data.get("cliente"),
                 comentario=form.cleaned_data.get("comentario"),

             )
            # print(SALE)

        # si la lista de formularios es valida 
        if formset.is_valid():
            # Inicializamos total a 0 
            total = 0

            # para cada formulario de la lista de formularios
            for form in formset:
                producto = form.cleaned_data.get("producto")
                cantidad = form.cleaned_data.get("cantidad")

                # las siguientes dos lineas de codigo permiten que cuando se venta cierto producto , el stock de este producto disminuya segun la cantidad vendida del mismo.
                producto.stock = producto.stock - cantidad
                producto.save()

                if producto and cantidad:
                    # sum tiene el valor del precio del producto multiplicado por la cantidad  
                    sum = float(producto.pvp) * float(cantidad)

                    # total tendra la venta total, es un  acumulador 
                    total += sum

                    # SaleDetail  proviene del modulo models
                    Detalle_De_Venta(venta=VENTA, producto=producto, cantidad=cantidad).save()

            # if 5 > 2 :
            #     maria = "hola" 
            # SALE.comment = maria

            VENTA.total = total
            VENTA.save()

        return redirect ("erp:Venta_Lista")



    context = {
        # este es el formulario normal 
        "form": form,
        "formset":formset,
    }


    return render(request, "4_crear_venta.html", context)


class Venta_Lista(PermissionRequiredMixin,ListView):
    model = Venta 
    template_name = "4_lista_venta.html" 
    permission_required = ("erp_v_i.view_venta") 

    # @method_decorator(login_required)
    # def dispatch(self, request, *args, **kwargs):
    #     return super().dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Lista Venta"
        context["url_crear"]= reverse_lazy("erp:Venta_Crear")

        context["entidad"] = "Venta"
        context["url_lista"] = reverse_lazy("erp:Venta_Lista")

        return context


@permission_required("erp_v_i.view_venta",raise_exception=True)
def Detalle_De_Venta_Ver(request,pk):
    venta_especifica = Venta.objects.get(id=pk)
    detalle_venta = Detalle_De_Venta.objects.filter(venta=venta_especifica) 



    context = {
        "venta_especifica":venta_especifica,
        "detalle_venta":detalle_venta,
        "url_lista":reverse_lazy("erp:Venta_Lista"),
        
    }
    return render(request, "5_detalle_de_venta_ver.html", context)


@permission_required("erp_v_i.delete_venta",raise_exception=True)
def Venta_Eliminar(request,pk):
    """ Esta Funcion Permite Eliminar La Venta y El Detalle de Venta Asociado """
    venta_especifica = Venta.objects.get(id=pk)
    detalle_venta = Detalle_De_Venta.objects.filter(venta=venta_especifica) 

    if request.method == "POST":
        venta_especifica.delete()
        detalle_venta.delete()
        return redirect("erp:Venta_Lista")

    context = {
        "venta_especifica":venta_especifica, 
        "detalle_venta":detalle_venta,
    }
    return render(request, "4_eliminar_venta.html", context)


