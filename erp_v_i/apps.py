from django.apps import AppConfig


class ErpVIConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'erp_v_i'
