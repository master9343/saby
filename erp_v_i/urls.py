from django.urls import path

# importacion de las vistas basadas en clase referentes al apartado de categoria 
from .views import Categoria_Lista, Categoria_Crear, Categoria_Editar, Categoria_Eliminar 

# importacion de las vista panel principal 
from .views import Panel_Principal 


# importacion de las vistas basadas en clase referentes al apartado de Producto 
from .views import Producto_Lista, Producto_Crear, Producto_Editar, Producto_Eliminar

# importacion de las vistas basadas en clase referentes al apartado de Cliente 
from .views import Cliente_Lista, Cliente_Crear, Cliente_Editar, Cliente_Eliminar, Cliente_Ver

# importacion de las vistas referentes al apartado de Venta y Detalle de Venta 
from .views import Venta_Crear, Venta_Lista, Detalle_De_Venta_Ver, Venta_Eliminar

# al colocar el app_name como erp permite especificar mas la url, por ende quedareia erp:categoria_lista cuando se use en el template
app_name = "erp"

urlpatterns = [

    # Vista Basadas en clase | Apartado Categorias 
    path('categoria/lista/', Categoria_Lista.as_view(), name="Categoria_Lista"),
    path('categoria/crear/', Categoria_Crear.as_view(), name="Categoria_Crear"),
    path('categoria/editar/<int:pk>/', Categoria_Editar.as_view(), name="Categoria_Editar"),
    path('categoria/eliminar/<int:pk>/', Categoria_Eliminar.as_view(), name="Categoria_Eliminar"),

    path('panel-principal/', Panel_Principal, name="Panel_Principal"),


    path('producto/lista/', Producto_Lista.as_view(), name="Producto_Lista"),
    path('producto/crear/', Producto_Crear.as_view(), name="Producto_Crear"),
    path('producto/editar/<int:pk>/', Producto_Editar.as_view(), name="Producto_Editar"),
    path('producto/eliminar/<int:pk>/', Producto_Eliminar.as_view(), name="Producto_Eliminar"),

    path('cliente/lista/', Cliente_Lista.as_view(), name="Cliente_Lista"),
    path('cliente/crear/', Cliente_Crear.as_view(), name="Cliente_Crear"),
    path('cliente/editar/<int:pk>/', Cliente_Editar.as_view(), name="Cliente_Editar"),
    path('cliente/eliminar/<int:pk>/', Cliente_Eliminar.as_view(), name="Cliente_Eliminar"),
    path('cliente/ver/<int:pk>/', Cliente_Ver, name='Cliente_Ver'),


    path('venta/crear/', Venta_Crear, name='Venta_Crear'),
    path('venta/lista/', Venta_Lista.as_view(), name="Venta_Lista"),

    path('detalleventa/ver/<int:pk>/', Detalle_De_Venta_Ver, name='Detalle_De_Venta_Ver'),

    path('venta/eliminar/<int:pk>/', Venta_Eliminar, name='Venta_Eliminar'),

]

