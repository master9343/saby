from django.db import models
from datetime import datetime

class Categoria(models.Model):
    nombre = models.CharField(max_length=90,verbose_name="Nombre",unique=True)
    descripcion = models.CharField(max_length=500, null=True, blank=True, verbose_name='Descripción')

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name ="Categoria"
        verbose_name_plural = "Categorias"
        ordering = ["id"]


class Producto(models.Model):
    nombre = models.CharField(max_length=160,verbose_name="Nombre",unique=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to="producto/%Y/%m/%d", null=True, blank=True)
    stock = models.PositiveIntegerField(default=1)
    pvp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)  

    def __str__(self):
        return (f"{self.nombre} ${self.pvp}") 

    class Meta:
        verbose_name = "Producto"
        verbose_name_plural = "Productos"
        ordering = ["id"]

class Cliente(models.Model):
    cedula_de_identidad = models.CharField(max_length=10, unique=True, verbose_name="Cedula De Identidad")
    nombres = models.CharField(max_length=90,verbose_name="Nombres")
    apellidos = models.CharField(max_length=90,verbose_name="Apellidos") 
    telefono = models.CharField(max_length=20, null=True, blank=True, verbose_name="Telefono")
    fecha_de_nacimiento = models.DateField(default=datetime.now, verbose_name="Fecha de Nacimiento")
    direccion = models.CharField(max_length=200, null=True, blank=True, verbose_name="Direccion")
    informacion_extra = models.CharField(max_length=200, null=True, blank=True, verbose_name="Informacion Extra del Cliente")

    def __str__(self):
        # return self.cedula_de_identidad
        return (f"{self.cedula_de_identidad} | {self.nombres}  {self.apellidos}") 

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"
        ordering = ["id"]


class Venta(models.Model):
    cliente = models.ForeignKey(Cliente, on_delete=models.SET_NULL, blank=True, null=True)
    fecha = models.DateTimeField(auto_now=True)
    comentario = models.TextField(default='', blank=True, null=True)
    total = models.FloatField(default=0)

class Detalle_De_Venta(models.Model):
    venta = models.ForeignKey(Venta, on_delete=models.SET_NULL, blank=True, null=True)
    producto = models.ForeignKey(Producto, on_delete=models.SET_NULL, blank=True, null=True)
    cantidad = models.IntegerField(default=1)

    





