from django.forms import ModelForm, TextInput, Textarea, NumberInput
from .models import Categoria, Producto, Cliente, Venta, Detalle_De_Venta
from django.forms import formset_factory

class Categoria_Formulario(ModelForm):
    class Meta:
        model = Categoria
        # fields representa las columnas que se mostraran en el formulario
        fields = '__all__'
        widgets = {
                "nombre": TextInput(
                    attrs={
                        "placeholder":"Ingrese Nombre",
                        "autofocus":"autofocus",
                    }
                ),
                "descripcion": Textarea(
                    attrs={
                        "placeholder":"Ingrese Descripcion",
                    }
                )
        }

class Producto_Formulario(ModelForm):
    class Meta:
        model = Producto 
        fields = "__all__"
        widgets = {
                "nombre":TextInput(
                    attrs={
                        "placeholder":"Ingrese Nombre",
                    }
                ),
                "pvp":NumberInput(
                    attrs={
                        "min":"0",
                    }
                ),
        }

class Cliente_Formulario(ModelForm):
    class Meta:
        model = Cliente 
        fields = "__all__"
        widgets = {
                "cedula_de_identidad":TextInput(
                    attrs={
                        "placeholder":"Ingrese Cedula de Identidad del Cliente",
                    }
                ),
                "nombres":TextInput(
                    attrs={
                        "placeholder":"Ingrese Nombres del Cliente",
                    }
                ),
                "apellidos":TextInput(
                    attrs={
                        "placeholder":"Ingrese Apellidos del Cliente",
                    }
                ),
                "telefono":TextInput(
                    attrs={
                        "placeholder":"Ingrese Teléfono del Cliente",
                    }
                ),
                "direccion": Textarea(
                    attrs={
                        "placeholder":"Ingrese dirección del Cliente",
                        "rows":"3"
                    }
                ),
                "informacion_extra": Textarea(
                    attrs={
                        "placeholder":"Ingrese Informacion Extra del Cliente. Ejemplo: Correo, Otra Dirección, Etc...",
                        "rows":"3"
                    }
                )
        }

class Venta_Formulario(ModelForm):
    class Meta:
        model = Venta
        fields = ['cliente','comentario',]
        widgets = {
                # "cliente": TextInput(
                #     attrs={
                #         "placeholder":"Ingrese Nombre del Cliente",
                #         # "autofocus":"autofocus",
                #     }
                # ),
                "comentario": Textarea(
                    attrs={
                        "placeholder":"Ingrese Comentario (Opcional)",
                    }
                )
        }


class Detalle_De_Venta_Formulario(ModelForm):
    class Meta:
        model = Detalle_De_Venta
        fields = ['producto','cantidad',]
        # widgets = {
        #     'prod': Select(),
        #     'amount': TextInput(attrs={
        #         'placeholder': '0',
        #         'type': 'number',
        #     })
        # }

Detalle_De_Venta_FormSet = formset_factory(Detalle_De_Venta_Formulario, extra=2)


