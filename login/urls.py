from django.urls import path

# del modulo views importamos la clase Inicio_De_Sesion
from .views import Inicio_De_Sesion,LogoutView

urlpatterns = [
    path('', Inicio_De_Sesion.as_view(),name="Iniciar_Sesion"),
    path('logout', LogoutView.as_view(),name="Cerrar_Sesion"),
]

