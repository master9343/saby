from django.contrib.auth.views import LoginView,LogoutView

from django.shortcuts import redirect

# del modulo setting que esta contenido dentro de la carpeta config , importamos la variable LOGIN_REDIRECT_URL
from config.settings import LOGIN_REDIRECT_URL

class Inicio_De_Sesion(LoginView):
    template_name = "login.html"

    
    def dispatch(self, request, *args, **kwargs):
        # Si el usuario ya esta autenticadoi, lo redireciorara a /erp/categoria/lista
        if request.user.is_authenticated:
            return redirect(LOGIN_REDIRECT_URL)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Iniciar Sesion"
        return context

