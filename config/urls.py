from django.contrib import admin
from django.urls import include, path

# De la app homepage y de su modulo views importamos la clase Index
from homepage.views import Index


# Las siguientes dos lineas de codigo hace que sea posible trabajar durante el desarrollo de la apliacion con archivos multimedia 
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('erp/', include('erp_v_i.urls')),
    path('', Index.as_view(), name="index"),
    path('login/',include('login.urls')),
]

# La siguiente linea de codigo hace que sea posible trabajar durante el desarrollo de la apliacion con archivos multimedia 
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
